FROM redis:alpine

RUN apk update &&\
    apk add bash --no-cache && \
    rm -rf /var/cache/apk/*